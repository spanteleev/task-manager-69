package ru.tsc.panteleev.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractRequest;

@Setter
@Getter
@NoArgsConstructor
public class UserLoginRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    public UserLoginRequest(@Nullable String login, @Nullable String password) {
        this.login = login;
        this.password = password;
    }

}
