package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.dto.model.TaskDto;

import java.util.List;

@Repository
public interface ITaskDtoRepository extends JpaRepository<TaskDto, String> {

    @NotNull
    List<TaskDto> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    TaskDto findByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<TaskDto> findAllByUserId(@NotNull String userId);

    @NotNull
    @Query("SELECT t FROM TaskDto t WHERE userId = :userId ORDER BY :sortColumn")
    List<TaskDto> findAllByUserIdSort(@Nullable @Param("userId") String userId,
                                      @Nullable @Param("sortColumn") String sortColumn);

    long countByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(String userId, String id);

    void deleteByUserId(String userId);

}
