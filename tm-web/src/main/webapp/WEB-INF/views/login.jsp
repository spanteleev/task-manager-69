<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<style>
        h3 {
            font-size: 1.8em;
        }
        td {
            font-size: 1.4em;
            padding: 5px;
        }
        input {
            font-size: 1.4em;
            padding: 5px;
        }
</style>

<html>
    <head>
        <title>Login</title>
    </head>
    <body onload='document.f.username.focus();'>
        <form name='f' action='/auth' method='POST'>
            <table width="600" border="1" cellpadding="10" cellspacing="0" align="center" >
                <tr>
                    <td colspan="2" align="center" >
                        <h3>Task Manager</h3>
                    </td>
                </tr>
                <tr>
                    <td align="right">Login:</td>
                    <td><input type='text' name='username' value=''></td>
                </tr>
                <tr>
                    <td align="right">Password:</td>
                    <td><input type='password' name='password'/></td>
                </tr>
                <tr>
                    <td colspan='2' align="center" >
                        <input name="submit" type="submit" value="Login"/>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>

