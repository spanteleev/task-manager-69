package ru.tsc.panteleev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(@NotNull String login);

    User findByEmail(@NotNull String email);

}
