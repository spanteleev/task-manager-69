package ru.tsc.panteleev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;

import java.util.Date;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDto> {

    @NotNull
    ProjectDto create(@Nullable String userId,
                      @Nullable String name,
                      @Nullable String description,
                      @Nullable Date dateBegin,
                      @Nullable Date dateEnd
    );

    @NotNull
    ProjectDto updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

}
