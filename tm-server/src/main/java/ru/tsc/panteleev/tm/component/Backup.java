package ru.tsc.panteleev.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.api.service.IDomainService;
import ru.tsc.panteleev.tm.service.DomainService;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Getter
@Component
public class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private IDomainService domainService;

    public void load() {
        if (!Files.exists(Paths.get(DomainService.FILE_BACKUP))) return;
        getDomainService().loadDataBackup();
    }

    public void save() {
        getDomainService().saveDataBackup();
    }

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

}
